library(tidyverse)
#load state names
state_names <- c("New South Wales", "Victoria", "Queensland", "South Australia", 
                 "Western Australia", "Tasmania")
#define a function to get the letters that are in at least one other state and not that state
get_possible_letters <- function(state, all_letters = TRUE){
  #What are the letters of the state
  state_letters <- unique(tolower(unlist(strsplit(gsub(pattern = " ", "", state), split = ""))))
  if(all_letters){
  possible_letters <- paste(sort(unique(letters[!letters %in% c(state_letters, " ")])), sep ="", collapse = "")} else {
  #what are the letters of all other states
  state_names_other <- state_names[state_names != state]
  other_state_letters <- unique(tolower(unlist(strsplit(state_names_other, split = ""))))
  #Find the possible letters 
  possible_letters <- paste(sort(unique(other_state_letters[!other_state_letters %in% c(state_letters, " ")])), sep ="", collapse = "")
  }
  return(possible_letters)
}
#Function to sort a word into its unique letters
sortsplit <- function(x) {paste(sort(unique(tolower(unlist(strsplit(gsub(pattern = " ", "", x), split = ""))))), sep ="", collapse = "")}
#Find all combinations of the "possible letters" for a state
find_combinations <- function (n, vector) {
  combinations <- combn(x = vector, m = n, simplify = F)
  collapsed_combinations <- unique(map_chr(combinations, paste, collapse = "", sep = ""))
  return(collapsed_combinations)
}
#Function that applies the above functions to generate all potential word combos 
generate_combos <- function(state, all_letters = TRUE){
  string <- get_possible_letters(state, all_letters)
  max_n <- nchar(string)
  vector <-  unlist(strsplit(string, split = ""))
  all_combinations <-  tibble(state = state,
                              letters = unique(unlist(map(1:max_n, find_combinations, vector))))
  return(all_combinations)
}

#Find all possible combinations of possible letters  - using all letters, not just the letters of the other states
all_state_combos_all_letters <- map_dfr(state_names, generate_combos)
unique_state_combos_all_letters  <- all_state_combos_all_letters[!(duplicated(all_state_combos_all_letters$letters)|duplicated(all_state_combos_all_letters$letters, fromLast=TRUE)), ]
#Find all possible combinations of possible letters  - using just the letters of the other states (i.e. not q)
all_state_combos <- map_dfr(state_names, generate_combos, all_letters = FALSE)
unique_state_combos  <- all_state_combos[!(duplicated(all_state_combos$letters)|duplicated(all_state_combos$letters, fromLast=TRUE)), ]

#load words
words <- readr::read_csv("https://norvig.com/ngrams/word.list", 
                         col_names = FALSE)
#Split words into letters and sortsplit them
words_letters <- words %>% 
  rename(word = X1) %>% 
  mutate(letters = map_chr(.x = word, sortsplit))
#inner join with unique state combos to find matches between possible letter combinations and words
results <- inner_join(unique_state_combos, words_letters, by = "letters")
results_all_letters <- inner_join(unique_state_combos_all_letters, words_letters, by = "letters")
#answer 
answer <- results %>% 
  mutate(word_length = nchar(word)) %>% 
  arrange(desc(word_length)) %>% 
  filter(word_length == max(word_length))
#extra credit 
extra_credit <- results %>% 
  mutate(word_length = nchar(word)) %>% 
  group_by(state) %>% 
  summarise(n = n(), 
            average_length = mean(word_length)) %>% 
  arrange(desc(n))

#answer 
answer_all_letters <- results_all_letters %>% 
  mutate(word_length = nchar(word)) %>% 
  arrange(desc(word_length)) %>% 
  filter(word_length == max(word_length))
#extra credit 
extra_credit_all_letters <- results_all_letters %>% 
  mutate(word_length = nchar(word)) %>% 
  group_by(state) %>% 
  summarise(n = n(), 
            average_length = mean(word_length)) %>% 
  arrange(desc(n))

#quibbles - there are 517 extra mackerels if you include q
quibbles <- anti_join(results_all_letters, results) %>% 
  mutate(word_length = nchar(word)) %>% 
  arrange(desc(word_length)) 

